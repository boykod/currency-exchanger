package ua.motionman.currencyexchanger.domain.repository

import ua.motionman.currencyexchanger.domain.models.ExchangeRateDomain

interface CurrencyExchangeRepository {

    suspend fun getCurrencyExchangeRates(): ExchangeRateDomain

}