package ua.motionman.currencyexchanger.domain.models

data class ExchangeRateDomain(
    val baseCurrency: String,
    val date: String,
    val rates: Map<Currency, Double>,
)

enum class Currency {
    USD,
    EUR,
    GBP,
}
