package ua.motionman.currencyexchanger

import android.app.Application
import ua.motionman.currencyexchanger.core.di.AppModule
import ua.motionman.currencyexchanger.core.di.AppModuleImpl

class CurrencyExchangerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        appModule = AppModuleImpl()
    }

    companion object {
        lateinit var appModule: AppModule
    }

}