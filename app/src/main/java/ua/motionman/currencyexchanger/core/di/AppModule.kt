package ua.motionman.currencyexchanger.core.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.ktor.client.HttpClient
import ua.motionman.currencyexchanger.core.ktor.ApiClient
import ua.motionman.currencyexchanger.data.datasource.CurrencyExchangeDataSource
import ua.motionman.currencyexchanger.data.remote.api.CurrencyExchangeService
import ua.motionman.currencyexchanger.data.remote.datasource.CurrencyExchangeRemoteDataSource
import ua.motionman.currencyexchanger.data.repository.CurrencyExchangeRepositoryImpl
import ua.motionman.currencyexchanger.domain.repository.CurrencyExchangeRepository

interface AppModule {

    val httpClient: HttpClient
    val currencyExchangeService: CurrencyExchangeService
    val currencyExchangeDataSource: CurrencyExchangeDataSource.Remote
    val currencyExchangeRepository: CurrencyExchangeRepository

}

class AppModuleImpl : AppModule {

    override val httpClient: HttpClient by lazy {
        ApiClient.createClient()
    }
    override val currencyExchangeService: CurrencyExchangeService by lazy {
        CurrencyExchangeService(httpClient)
    }
    override val currencyExchangeDataSource: CurrencyExchangeDataSource.Remote by lazy {
        CurrencyExchangeRemoteDataSource(currencyExchangeService)
    }
    override val currencyExchangeRepository: CurrencyExchangeRepository by lazy {
        CurrencyExchangeRepositoryImpl(currencyExchangeDataSource)
    }

}

fun <VM : ViewModel> viewModelFactory(initializer: () -> VM): ViewModelProvider.Factory {
    return object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return initializer() as T
        }
    }
}