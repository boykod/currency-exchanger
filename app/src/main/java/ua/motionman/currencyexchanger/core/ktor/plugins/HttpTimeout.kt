package ua.motionman.currencyexchanger.core.ktor.plugins

import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngineConfig
import io.ktor.client.plugins.HttpTimeout

fun HttpClientConfig<HttpClientEngineConfig>.configureHttpTimeout() {
    install(HttpTimeout) {
        connectTimeoutMillis = 15000
        requestTimeoutMillis = 30000
    }
}