package ua.motionman.currencyexchanger.core.ktor

import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngineConfig
import io.ktor.client.engine.HttpClientEngineFactory
import io.ktor.client.engine.okhttp.OkHttp
import ua.motionman.currencyexchanger.core.ktor.plugins.configureContentNegotiation
import ua.motionman.currencyexchanger.core.ktor.plugins.configureDefaultRequest
import ua.motionman.currencyexchanger.core.ktor.plugins.configureHttpTimeout
import ua.motionman.currencyexchanger.core.ktor.plugins.configureLogging

object ApiClient {

    private val engine: HttpClientEngineFactory<HttpClientEngineConfig> = OkHttp

    fun createClient() = HttpClient(engine) {
        configureContentNegotiation()
        configureLogging()
        configureHttpTimeout()
        configureDefaultRequest()
    }

}