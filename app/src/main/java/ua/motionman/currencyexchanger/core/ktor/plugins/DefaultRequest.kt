package ua.motionman.currencyexchanger.core.ktor.plugins

import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngineConfig
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.URLBuilder

fun HttpClientConfig<HttpClientEngineConfig>.configureDefaultRequest() {
    install(DefaultRequest) {
        val endpointUrlBuilder = URLBuilder("https://developers.paysera.com")

        url {
            protocol = endpointUrlBuilder.protocol
            host = endpointUrlBuilder.host
        }

        header(HttpHeaders.ContentType, ContentType.Application.Json)
    }
}