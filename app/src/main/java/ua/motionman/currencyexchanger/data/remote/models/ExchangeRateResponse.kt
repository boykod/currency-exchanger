package ua.motionman.currencyexchanger.data.remote.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ExchangeRateResponse(
    val base: String,
    val date: String,
    val rates: RatesResponse,
)

@Serializable
data class RatesResponse(
    @SerialName("USD") val usd: Double,
    @SerialName("EUR") val eur: Double,
    @SerialName("GBP") val gbp: Double,
)
