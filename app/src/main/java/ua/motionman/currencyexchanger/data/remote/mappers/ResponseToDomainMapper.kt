package ua.motionman.currencyexchanger.data.remote.mappers

import ua.motionman.currencyexchanger.data.remote.models.ExchangeRateResponse
import ua.motionman.currencyexchanger.data.remote.models.RatesResponse
import ua.motionman.currencyexchanger.domain.models.Currency
import ua.motionman.currencyexchanger.domain.models.ExchangeRateDomain

fun ExchangeRateResponse.toDomain(): ExchangeRateDomain {
    return ExchangeRateDomain(
        baseCurrency = base,
        date = date,
        rates = rates.toDomain()
    )
}

fun RatesResponse.toDomain() = mapOf(
    Currency.USD to usd,
    Currency.EUR to eur,
    Currency.GBP to gbp
)