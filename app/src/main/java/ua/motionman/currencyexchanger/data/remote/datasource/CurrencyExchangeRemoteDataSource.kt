package ua.motionman.currencyexchanger.data.remote.datasource

import ua.motionman.currencyexchanger.data.datasource.CurrencyExchangeDataSource
import ua.motionman.currencyexchanger.data.remote.api.CurrencyExchangeService
import ua.motionman.currencyexchanger.data.remote.mappers.toDomain
import ua.motionman.currencyexchanger.domain.models.ExchangeRateDomain

class CurrencyExchangeRemoteDataSource(
    private val service: CurrencyExchangeService,
) : CurrencyExchangeDataSource.Remote {

    override suspend fun getCurrencyExchangeRates(): ExchangeRateDomain {
        return service
            .getCurrencyExchangeRates()
            .toDomain()
    }

}