package ua.motionman.currencyexchanger.data.remote.api

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import ua.motionman.currencyexchanger.data.remote.models.ExchangeRateResponse

class CurrencyExchangeService(
    private val httpClient: HttpClient,
) {

    private val serviceUrl: String
        get() = "/tasks/api/currency-exchange-rates"

    suspend fun getCurrencyExchangeRates(): ExchangeRateResponse {
        return httpClient
            .get(serviceUrl)
            .body()
    }

}