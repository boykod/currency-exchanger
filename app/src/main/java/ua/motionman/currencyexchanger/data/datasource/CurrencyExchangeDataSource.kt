package ua.motionman.currencyexchanger.data.datasource

import ua.motionman.currencyexchanger.domain.models.ExchangeRateDomain

interface CurrencyExchangeDataSource {

    interface Remote {
        suspend fun getCurrencyExchangeRates(): ExchangeRateDomain
    }

}