package ua.motionman.currencyexchanger.data.repository

import ua.motionman.currencyexchanger.data.datasource.CurrencyExchangeDataSource
import ua.motionman.currencyexchanger.domain.models.ExchangeRateDomain
import ua.motionman.currencyexchanger.domain.repository.CurrencyExchangeRepository

class CurrencyExchangeRepositoryImpl(
    private val remote: CurrencyExchangeDataSource.Remote,
) : CurrencyExchangeRepository {

    override suspend fun getCurrencyExchangeRates(): ExchangeRateDomain {
        return remote.getCurrencyExchangeRates()
    }

}