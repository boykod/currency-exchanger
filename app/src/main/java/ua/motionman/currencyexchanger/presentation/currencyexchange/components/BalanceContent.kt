package ua.motionman.currencyexchanger.presentation.currencyexchange.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ua.motionman.currencyexchanger.domain.models.Currency

@Composable
fun BalanceContent(
    modifier: Modifier = Modifier,
    balances: Map<Currency, Double>,
) {
    Column(
        modifier = modifier,
    ) {
        Text(
            text = "My Balances",
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp,
        )

        LazyRow(
            modifier = Modifier
                .padding(vertical = 8.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            items(balances.toList()) { (currency, balance) ->
                Text(
                    text = "$balance ${currency.name}",
                    color = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun BalanceContentPreview() {
    BalanceContent(
        balances = mapOf(
            Currency.USD to 100.0,
            Currency.EUR to 200.0,
            Currency.GBP to 300.0,
        )
    )
}