package ua.motionman.currencyexchanger.presentation.currencyexchange

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ua.motionman.currencyexchanger.domain.repository.CurrencyExchangeRepository

class CurrencyExchangeViewModel(
    private val currencyExchangeRepository: CurrencyExchangeRepository,
) : ViewModel() {

    private val _uiState = MutableStateFlow(CurrencyExchangeState())
    val uiState = _uiState.asStateFlow()

    private val _sideEffect = MutableSharedFlow<CurrencyExchangeSideEffect>(
        extraBufferCapacity = 1
    )
    val sideEffect = _sideEffect.asSharedFlow()

    fun consumeEvent(event: CurrencyExchangeEvent) {
        val currentState = _uiState.value

        when (event) {
            is CurrencyExchangeEvent.LoadCurrenciesRates ->
                loadCurrenciesRates()

            is CurrencyExchangeEvent.OnChangeSellValue -> {
                val state = currentState.copy(
                    exchanger = currentState.exchanger.copy(
                        sell = currentState.exchanger.sell.copy(value = event.value)
                    )
                )
                _uiState.tryEmit(applyExchange(state))
            }

            is CurrencyExchangeEvent.OnChangeSellCurrency -> {
                val state = currentState.copy(
                    exchanger = currentState.exchanger.copy(
                        sell = currentState.exchanger.sell.copy(currency = event.currency)
                    )
                )
                _uiState.tryEmit(applyExchange(state))
            }

            is CurrencyExchangeEvent.OnChangeReceiveCurrency -> {
                val state = currentState.copy(
                    exchanger = currentState.exchanger.copy(
                        receive = currentState.exchanger.receive.copy(currency = event.currency)
                    )
                )
                _uiState.tryEmit(applyExchange(state))
            }

            is CurrencyExchangeEvent.OnSubmitClick -> {
                _uiState.update { state ->
                    val balance = state.balance.toMutableMap()

                    val sellValue = state.exchanger.sell.value.toDoubleOrNull() ?: 0.0
                    val sellCurrencyBalance =
                        (balance[state.exchanger.sell.currency] ?: 0.0) - sellValue
                    balance[state.exchanger.sell.currency] = sellCurrencyBalance

                    val receiveValue = state.exchanger.receive.value.toDoubleOrNull() ?: 0.0
                    val receiveCurrencyBalance =
                        (balance[state.exchanger.receive.currency] ?: 0.0) + receiveValue
                    balance[state.exchanger.receive.currency] = receiveCurrencyBalance

                    state.copy(
                        balance = balance,
                        exchanger = state.exchanger.copy(
                            sell = state.exchanger.sell.copy(value = ""),
                            receive = state.exchanger.receive.copy(value = ""),
                        )
                    )
                }

                val sell = currentState.exchanger.sell
                val receive = currentState.exchanger.receive
                val message =
                    "You have converted ${sell.value} ${sell.currency} to ${receive.value} ${receive.currency}."
                _sideEffect.tryEmit(
                    CurrencyExchangeSideEffect.ShowExchangeSuccessDialog(message)
                )
            }
        }
    }

    private fun loadCurrenciesRates() = viewModelScope.launch {
        val rates = currencyExchangeRepository.getCurrencyExchangeRates()
        _uiState.update { it.copy(rates = rates.rates) }
    }

    private fun applyExchange(
        state: CurrencyExchangeState,
    ): CurrencyExchangeState {
        val exchanger = state.exchanger
        val sell = exchanger.sell
        val sellValue = sell.value.toDoubleOrNull() ?: 0.0
        val receiveRate = state.rates[exchanger.receive.currency] ?: 0.0
        val receive = exchanger.receive.copy(
            value = String.format("%.2f", sellValue * receiveRate)
        )

        return state.copy(
            exchanger = exchanger.copy(
                sell = sell,
                receive = receive,
            )
        )
    }

}