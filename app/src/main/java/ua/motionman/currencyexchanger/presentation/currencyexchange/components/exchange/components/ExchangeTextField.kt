package ua.motionman.currencyexchanger.presentation.currencyexchange.components.exchange.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp


@Composable
fun ExchangeTextField(
    modifier: Modifier = Modifier,
    value: String,
    enabled: Boolean,
    onValueChange: (String) -> Unit,
) {
    TextField(
        modifier = modifier,
        value = value,
        placeholder = {
            ExchangePlaceholder(
                modifier = Modifier
                    .fillMaxWidth(),
            )
        },
        onValueChange = { onValueChange(it) },
        readOnly = enabled.not(),
        singleLine = true,
        textStyle = TextStyle(
            fontSize = 16.sp,
            textAlign = TextAlign.End
        ),
        colors = OutlinedTextFieldDefaults.colors(
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent,
            disabledBorderColor = Color.Transparent,
            unfocusedContainerColor = Color.Transparent,
            focusedContainerColor = Color.Transparent,
            focusedTextColor = Color.Black,
            unfocusedTextColor = Color.Black,
            disabledTextColor = Color.Black,
        ),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number,
        )
    )
}

@Composable
private fun ExchangePlaceholder(
    modifier: Modifier = Modifier,
) {
    Text(
        modifier = modifier,
        text = "0.00",
        style = TextStyle(
            fontSize = 16.sp,
            textAlign = TextAlign.End
        )
    )
}

@Preview(showBackground = true)
@Composable
private fun ExchangeTextFieldPreview() {
    ExchangeTextField(
        value = "100.00",
        enabled = true,
        onValueChange = {}
    )
}