package ua.motionman.currencyexchanger.presentation.currencyexchange

import androidx.compose.runtime.Stable
import ua.motionman.currencyexchanger.domain.models.Currency

@Stable
data class CurrencyExchangeState(
    val balance: Map<Currency, Double> = initBalance(),
    val exchanger: Exchanger = Exchanger(),
    val rates: Map<Currency, Double> = emptyMap(),
) {

    fun isExchangeValid(): Boolean {
        val sellBalance = balance[exchanger.sell.currency] ?: 0.0
        val sellValue = exchanger.sell.value.toDoubleOrNull() ?: 0.0

        if (sellValue <= 0.0 || sellBalance <= 0.0) return false

        return sellValue <= sellBalance
    }

}

@Stable
data class Exchanger(
    val sell: ExchangeState = ExchangeState("", Currency.EUR),
    val receive: ExchangeState = ExchangeState("", Currency.USD),
)

@Stable
data class ExchangeState(
    val value: String,
    val currency: Currency,
)

private fun initBalance(): Map<Currency, Double> {
    return mapOf(Currency.EUR to 1000.0)
}

sealed interface CurrencyExchangeEvent {
    data object LoadCurrenciesRates : CurrencyExchangeEvent
    data class OnChangeSellValue(val value: String) : CurrencyExchangeEvent
    data class OnChangeSellCurrency(val currency: Currency) : CurrencyExchangeEvent
    data class OnChangeReceiveCurrency(val currency: Currency) : CurrencyExchangeEvent
    data object OnSubmitClick : CurrencyExchangeEvent
}

sealed interface CurrencyExchangeSideEffect {
    data class ShowExchangeSuccessDialog(val message: String) : CurrencyExchangeSideEffect
}