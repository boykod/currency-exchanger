package ua.motionman.currencyexchanger.presentation.currencyexchange

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ua.motionman.currencyexchanger.CurrencyExchangerApplication
import ua.motionman.currencyexchanger.core.di.viewModelFactory
import ua.motionman.currencyexchanger.domain.models.Currency
import ua.motionman.currencyexchanger.presentation.currencyexchange.components.BalanceContent
import ua.motionman.currencyexchanger.presentation.currencyexchange.components.CurrencyConvertedAlertDialog
import ua.motionman.currencyexchanger.presentation.currencyexchange.components.exchange.ExchangeContent

@Composable
private fun getViewModel(): CurrencyExchangeViewModel {
    val viewModel: CurrencyExchangeViewModel = viewModel(
        factory = viewModelFactory {
            CurrencyExchangeViewModel(
                currencyExchangeRepository = CurrencyExchangerApplication.appModule.currencyExchangeRepository
            )
        }
    )

    return viewModel
}

@Composable
fun CurrencyExchangeScreen() {
    val viewModel: CurrencyExchangeViewModel = getViewModel()

    var showAlertDialog by remember { mutableStateOf(false) }
    var alertDialogMessage by remember { mutableStateOf("") }

    LaunchedEffect(Unit) {
        viewModel.consumeEvent(CurrencyExchangeEvent.LoadCurrenciesRates)

        viewModel.sideEffect.collect { effect ->
            when (effect) {
                is CurrencyExchangeSideEffect.ShowExchangeSuccessDialog -> {
                    alertDialogMessage = effect.message
                    showAlertDialog = true
                }
            }
        }
    }

    CurrencyExchangeContent(
        state = viewModel.uiState.collectAsState(),
        onEvent = viewModel::consumeEvent
    )

    if (showAlertDialog) {
        CurrencyConvertedAlertDialog(
            message = alertDialogMessage,
            onDismissRequest = { showAlertDialog = false }
        )
    }
}

@Composable
private fun CurrencyExchangeContent(
    state: State<CurrencyExchangeState>,
    onEvent: (CurrencyExchangeEvent) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        BalanceContent(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            balances = state.value.balance
        )

        ExchangeContent(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            state = state.value.exchanger,
            onEvent = onEvent,
        )

        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp),
            onClick = { onEvent(CurrencyExchangeEvent.OnSubmitClick) },
            enabled = state.value.isExchangeValid(),
        ) {
            Text(text = "Submit")
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun CurrencyExchangeContentPreview() {
    val state = remember {
        mutableStateOf(
            CurrencyExchangeState(
                balance = mapOf(
                    Currency.USD to 100.0,
                    Currency.EUR to 200.0,
                    Currency.GBP to 300.0,
                )
            )
        )
    }

    CurrencyExchangeContent(
        state = state,
        onEvent = {}
    )
}