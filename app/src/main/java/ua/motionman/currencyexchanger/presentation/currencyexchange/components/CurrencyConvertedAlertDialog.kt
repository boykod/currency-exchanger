package ua.motionman.currencyexchanger.presentation.currencyexchange.components

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import ua.motionman.currencyexchanger.R


@Composable
fun CurrencyConvertedAlertDialog(
    message: String,
    onDismissRequest: () -> Unit,
) {
    AlertDialog(
        onDismissRequest = { onDismissRequest() },
        title = { Text(text = stringResource(R.string.currency_converted_title)) },
        text = { Text(text = message) },
        confirmButton = {
            Button(onClick = { onDismissRequest() }) {
                Text(text = "Done")
            }
        }
    )
}

@Preview
@Composable
private fun CurrencyConvertedAlertDialogPreview() {
    CurrencyConvertedAlertDialog(
        message = "Currency converted successfully",
        onDismissRequest = { }
    )
}