package ua.motionman.currencyexchanger.presentation.currencyexchange.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowDown
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import ua.motionman.currencyexchanger.domain.models.Currency

@Composable
fun CurrencyDropDown(
    modifier: Modifier = Modifier,
    currency: Currency,
    onSelect: (Currency) -> Unit
) {
    var currencyDropDownMenu by remember {
        mutableStateOf(false)
    }

    Box(
        modifier = modifier
    ) {
        DropdownMenu(
            expanded = currencyDropDownMenu,
            onDismissRequest = { currencyDropDownMenu = false }
        ) {
            Currency.entries.forEach {
                CurrencyDropDownItem(
                    currency = it,
                    onClick = {
                        onSelect(it)
                        currencyDropDownMenu = false
                    }
                )
            }
        }

        TextWithIconEnd(
            title = currency.name,
            image = Icons.Outlined.KeyboardArrowDown,
            onClick = { currencyDropDownMenu = true }
        )
    }
}

@Composable
fun TextWithIconEnd(
    title: String,
    image: ImageVector,
    onClick: () -> Unit,
) {
    Row(
        modifier = Modifier
            .clickable { onClick() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = title,
            modifier = Modifier.padding(end = 8.dp)
        )
        Icon(
            imageVector = image,
            contentDescription = null
        )
    }
}

@Composable
fun CurrencyDropDownItem(
    modifier: Modifier = Modifier,
    currency: Currency,
    onClick: () -> Unit,
) {
    DropdownMenuItem(
        modifier = modifier,
        text = {
            Text(text = currency.name)
        },
        onClick = { onClick() }
    )
}