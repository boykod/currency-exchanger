package ua.motionman.currencyexchanger.presentation.currencyexchange.components.exchange

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ua.motionman.currencyexchanger.presentation.currencyexchange.CurrencyExchangeEvent
import ua.motionman.currencyexchanger.presentation.currencyexchange.ExchangeState
import ua.motionman.currencyexchanger.presentation.currencyexchange.Exchanger
import ua.motionman.currencyexchanger.presentation.currencyexchange.components.CurrencyDropDown
import ua.motionman.currencyexchanger.presentation.currencyexchange.components.exchange.components.ExchangeTextField

@Composable
fun ExchangeContent(
    modifier: Modifier = Modifier,
    state: Exchanger,
    onEvent: (CurrencyExchangeEvent) -> Unit,
) {
    Column(
        modifier = modifier,
    ) {
        Text(
            text = "Currency Exchange",
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp,
        )

        ExchangeRow(
            exchangeType = ExchangeType.SELL,
            state = state.sell,
            onEvent = onEvent,
        )

        Spacer(modifier = Modifier.height(16.dp))

        ExchangeRow(
            exchangeType = ExchangeType.RECEIVE,
            state = state.receive,
            onEvent = onEvent,
        )
    }
}

@Composable
fun ExchangeRow(
    exchangeType: ExchangeType,
    state: ExchangeState,
    onEvent: (CurrencyExchangeEvent) -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = exchangeType.getTitle(),
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.secondary,
        )

        ExchangeTextField(
            modifier = Modifier
                .weight(1f),
            value = state.value,
            enabled = exchangeType == ExchangeType.SELL,
            onValueChange = { onEvent(CurrencyExchangeEvent.OnChangeSellValue(it)) },
        )

        CurrencyDropDown(
            currency = state.currency,
            onSelect = {
                val event = when (exchangeType) {
                    ExchangeType.SELL -> CurrencyExchangeEvent.OnChangeSellCurrency(it)
                    ExchangeType.RECEIVE -> CurrencyExchangeEvent.OnChangeReceiveCurrency(it)
                }

                onEvent(event)
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun ExchangeContentPreview() {
    ExchangeContent(
        state = Exchanger(),
        onEvent = {},
    )
}

enum class ExchangeType {
    SELL,
    RECEIVE,
}

private fun ExchangeType.getTitle(): String {
    return when (this) {
        ExchangeType.SELL -> "Sell"
        ExchangeType.RECEIVE -> "Receive"
    }
}